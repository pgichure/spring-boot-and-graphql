package com.pgichure.springsamples.graphql.repositories;

import package com.pgichure.springsamples.graphql.models.Book;

public interface BookRepository extends JpaRepository<Book, Long>{

}