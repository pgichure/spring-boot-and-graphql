package com.pgichure.springsamples.graphql.services;


import com.pgichure.springsamples.graphql.models.Author;

public interface AuthorServiceI{

     public Author save(Author author);

    public Author findById(Long id);

    public Author update(Author author);

    public Author deleteById(Long id);

    public List<Author> findAll();

}