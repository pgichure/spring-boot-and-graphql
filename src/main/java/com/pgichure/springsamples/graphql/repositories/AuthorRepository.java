package com.pgichure.springsamples.graphql.repositories;

import package com.pgichure.springsamples.graphql.models.Author;

public interface AuthorRepository extends JpaRepository<Author, String>{

}