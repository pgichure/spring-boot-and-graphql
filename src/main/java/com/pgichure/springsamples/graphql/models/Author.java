package com.pgichure.springsamples.graphql.models;

import lombok.Data;
import lombok.Builder;

@Data
@Builder
@Entity
@Table(name = "authors")
public class Author{

    private Long id;

    private String firstName;

    private String lastName;
    
}