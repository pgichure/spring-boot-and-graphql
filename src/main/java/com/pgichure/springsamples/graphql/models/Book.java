package com.pgichure.springsamples.graphql.models;

import lombok.Data;
import lombok.Builder;

@Data
@Builder
@Entity
@Table(name = "books")
public class Book{

    private Long id;

    private String name;

    private int pageCount;

    private String authorId;

    private String isbn;

}