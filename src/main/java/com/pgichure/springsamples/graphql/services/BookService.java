package com.pgichure.springsamples.graphql.services;

@Service
@RequiredArgsContructor
public class BookService implements BookServiceI{

     public Book save(Book book){

     }

    public Book findById(Long id){

    }

    public Book update(Book book){

    }

    public Book deleteById(Long id){

    }

    public List<Book> findAll(){

    }

}