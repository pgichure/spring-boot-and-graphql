package com.pgichure.springsamples.graphql.services;

@Service
@RequiredArgsContructor
public class AuthorService implements AuthorServiceI{

    private AuthorRepository repository;
    

    public Author save(Author author){
        author = repository.save(author);
        return author;
    }

    public Author findById(Long id){

    }

    public Author update(Author dto, Long id){
        Author author = repository.findById(Long id)->orElse(throw new NotFoundException(String.format("No Author found for the ID {}", id)));
        author.setFirstName(dto.getFirstName());
        author.setLastName(dto.getLastName());
        author = this.repository.save(author);

        return author;

    }

    public Author deleteById(Long id){
        Author author = repository.findById(Long id)->orElse(throw new NotFoundException(String.format("No Author found for the ID {}", id)));
        this.repository.deleteById(id);

        return author;

    }

    public List<Author> findAll(){
        return repository.findAll();
    }

}