package com.pgichure.springsamples.graphql.services;

import package com.pgichure.springsamples.graphql.models.Book;

public interface BookServiceI{

    public Book save(Book book);

    public Book findById(Long id);

    public Book update(Book book);

    public Book deleteById(Long id);

    public List<Book> findAll();

    public List<Book> findByAuthorId();

}