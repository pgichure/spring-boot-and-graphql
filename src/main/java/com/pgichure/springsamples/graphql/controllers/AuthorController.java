package com.pgichure.springsamples.graphql.controllers;

import com.pgichure.springsamples.graphql.services.AuthorServiceI;
import com.pgichure.springsamples.graphql.models.Author;

@Controller
@RequiredArgsContructor
public class AuthorController{

    private AuthorServiceI service;

    @QueryMapping
    public Author authorById(@Argument Long id) {
        return service.findById(id);
    }

    @MutationMapping
    public Author createAuthor(@Argument String firstName, @Argument String lastName) {

        Author author = new Author.builder
                            .firstName(firstName)
                            .lastName(lastName).build();

        author = service.save(author);
        return author;
    }

}