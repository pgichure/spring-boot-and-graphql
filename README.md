## Spring Boot & GraphQL
In this example, we will create a GraphQL server in Java using Spring for GraphQ,which offers an easier way to get a GraphQL server running in a very short time.  
GraphQL is a query language for your API, and a server-side runtime for executing queries using a type system you define for your data. It is an alternative to REST & SOAP and in most cases will replace them and not sit alongside them.

### Setting up
Add the following maven dependencies:

```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-graphql</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency> 
```  

The GraphQL Boot starter processes GraphQL Schema files to build the  by automatically reading the schema files and configuring approprite beans. Create the “.graphqls” or “.gqls” schema files under src/main/resources/graphql/** location, and Spring Boot will pick them up automatically. We can customize the locations with spring.graphql.schema.locations and the file extensions with spring.graphql.schema.file-extensions configuration properties under application.properties.  

Spring GraphQL comes with a default GraphQL page that is exposed at /graphiql endpoint. The endpoint is disabled by default but it can be turned on by enabling the spring.graphql.graphiql.enabled property. This provides a very useful in-browser tool to write and test queries, particularly during development and testing.  

