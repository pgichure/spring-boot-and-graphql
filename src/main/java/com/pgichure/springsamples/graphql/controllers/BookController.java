package com.pgichure.springsamples.graphql.controllers;


import com.pgichure.springsamples.graphql.services.BookServiceI;
import com.pgichure.springsamples.graphql.models.Book;

@Controller
@RequiredArgsContructor
public class BookController{

    private BookServiceI service;

    @QueryMapping
    public Book bookById(@Argument Long id) {
        return service.findById(id);
    }

    @MutationMapping
    public Book createBook(@Argument String name, @Argument String pageCount, @Argument String authorId, @Argument String isbn) {

        Book book = new Book.builder
                            .name(name)
                            .pageCount(pageCount)
                            .authorById(authorById)
                            .isbn(isbn).build();

        book =service.save(book);

        return book;
    }
    
}